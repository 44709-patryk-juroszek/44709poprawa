using UnityEngine.SceneManagement;
using UnityEngine;



public class GameManager : MonoBehaviour

{
    bool gameHasEnded = false;
    public float restartDelay = 1f;
    

    public GameObject completeUI;        
    
    public void CompleteLevel()
    {
        completeUI.SetActive(true);
    }
    public void GameOver()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("Game Over");
            Invoke("Restart", restartDelay);
        }
        

    }
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }
}
